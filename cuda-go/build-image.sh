set -e

GO_VER=$1
CUDA_VER=11.1.1

if [ -z ${skip_rollback} ]; then 
# Retag current prod image with -rollback suffix, also keeping it with the original name on prod for now
docker pull docker-registry.xx.network/elixxir/sysadmin/backend-ci:go$GO_VER-cuda$CUDA_VER
docker tag docker-registry.xx.network/elixxir/sysadmin/backend-ci:go$GO_VER-cuda$CUDA_VER \
           docker-registry.xx.network/elixxir/sysadmin/backend-ci:go$GO_VER-cuda$CUDA_VER-rollback
docker push docker-registry.xx.network/elixxir/sysadmin/backend-ci:go$GO_VER-cuda$CUDA_VER-rollback
fi

# Build new version and push to replace
docker build --build-arg GO_VER_FULL=$2 --build-arg GO_VER=$1 --no-cache -t docker-registry.xx.network/elixxir/sysadmin/backend-ci:go$GO_VER-cuda$CUDA_VER .
docker push docker-registry.xx.network/elixxir/sysadmin/backend-ci:go$GO_VER-cuda$CUDA_VER

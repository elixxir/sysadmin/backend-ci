mc alias set elixxir-s3 $ARTIFACT_S3_ENDPOINT $ARTIFACT_S3_KEY $ARTIFACT_S3_SECRET
export B2BSUM=$(/hash-file.sh $2)
mc cp $2 elixxir-s3/$ARTIFACT_S3_BUCKET/$1/$B2BSUM
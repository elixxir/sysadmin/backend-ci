# backend-ci

Docker image files for the backend engineering team. These are used primarily on our GitLab Runner CI servers as the environment used to automatically build and test our software.
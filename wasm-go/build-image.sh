set -e

GO_VER=$1

if [ -z ${skip_rollback} ]; then 
# Retag current prod image with -rollback suffix, also keeping it with the original name on prod for now
docker pull docker-registry.xx.network/elixxir/sysadmin/backend-ci:go$GO_VER-wasm
docker tag docker-registry.xx.network/elixxir/sysadmin/backend-ci:go$GO_VER-wasm \
           docker-registry.xx.network/elixxir/sysadmin/backend-ci:go$GO_VER-wasm-rollback
docker push docker-registry.xx.network/elixxir/sysadmin/backend-ci:go$GO_VER-wasm-rollback
fi

# Build new version and push to replace
docker build --build-arg GO_VER=$1 -t docker-registry.xx.network/elixxir/sysadmin/backend-ci:go$GO_VER-wasm .
docker push docker-registry.xx.network/elixxir/sysadmin/backend-ci:go$GO_VER-wasm

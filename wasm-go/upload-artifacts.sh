if [ "$CI_COMMIT_REF_NAME" != "master" ] && [ "$CI_COMMIT_REF_NAME" != "release" ] && [ "$ARTIFACT_BRANCH_BYPASS" != "yes" ]; then
  echo 'Branch is not master or release, aborting script';
  exit 0;
fi

cd $1
mc alias set elixxir-s3 $ARTIFACT_S3_ENDPOINT $ARTIFACT_S3_KEY $ARTIFACT_S3_SECRET
mc cp * elixxir-s3/$ARTIFACT_S3_BUCKET/$CI_COMMIT_REF_NAME/